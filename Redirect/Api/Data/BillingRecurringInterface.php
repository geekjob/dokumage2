<?php


namespace Doku\Redirect\Api\Data;

interface BillingRecurringInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const CUSTOMERID = 'customerId';
    const BILLINGRECURRING_ID = 'billingrecurring_id';
    const ORDERNO = 'orderNo';
    const CHARGEAMOUNT = 'chargeamount';

    /**
     * Get billingrecurring_id
     * @return string|null
     */
    public function getBillingrecurringId();

    /**
     * Set billingrecurring_id
     * @param string $billingrecurringId
     * @return \Doku\Redirect\Api\Data\BillingRecurringInterface
     */
    public function setBillingrecurringId($billingrecurringId);

    /**
     * Get customerId
     * @return string|null
     */
    public function getCustomerId();

    /**
     * Set customerId
     * @param string $customerId
     * @return \Doku\Redirect\Api\Data\BillingRecurringInterface
     */
    public function setCustomerId($customerId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Doku\Redirect\Api\Data\BillingRecurringExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Doku\Redirect\Api\Data\BillingRecurringExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Doku\Redirect\Api\Data\BillingRecurringExtensionInterface $extensionAttributes
    );

    /**
     * Get orderNo
     * @return string|null
     */
    public function getOrderNo();

    /**
     * Set orderNo
     * @param string $orderNo
     * @return \Doku\Redirect\Api\Data\BillingRecurringInterface
     */
    public function setOrderNo($orderNo);

    /**
     * Get chargeamount
     * @return string|null
     */
    public function getChargeamount();

    /**
     * Set chargeamount
     * @param string $chargeamount
     * @return \Doku\Redirect\Api\Data\BillingRecurringInterface
     */
    public function setChargeamount($chargeamount);
}
