<?php


namespace Doku\Redirect\Api\Data;

interface BillingRecurringSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get BillingRecurring list.
     * @return \Doku\Redirect\Api\Data\BillingRecurringInterface[]
     */
    public function getItems();

    /**
     * Set customerId list.
     * @param \Doku\Redirect\Api\Data\BillingRecurringInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
