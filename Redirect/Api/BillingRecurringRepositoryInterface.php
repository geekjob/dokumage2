<?php


namespace Doku\Redirect\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface BillingRecurringRepositoryInterface
{

    /**
     * Save BillingRecurring
     * @param \Doku\Redirect\Api\Data\BillingRecurringInterface $billingRecurring
     * @return \Doku\Redirect\Api\Data\BillingRecurringInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Doku\Redirect\Api\Data\BillingRecurringInterface $billingRecurring
    );

    /**
     * Retrieve BillingRecurring
     * @param string $billingrecurringId
     * @return \Doku\Redirect\Api\Data\BillingRecurringInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($billingrecurringId);

    /**
     * Retrieve BillingRecurring matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Doku\Redirect\Api\Data\BillingRecurringSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete BillingRecurring
     * @param \Doku\Redirect\Api\Data\BillingRecurringInterface $billingRecurring
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Doku\Redirect\Api\Data\BillingRecurringInterface $billingRecurring
    );

    /**
     * Delete BillingRecurring by ID
     * @param string $billingrecurringId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($billingrecurringId);
}
