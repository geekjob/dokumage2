define(
    [
        'Magento_Checkout/js/view/payment/default',
        'jquery',
        'mage/url',
        'Magento_Checkout/js/model/url-builder',
    ],
    function (Component,$,url, urlBuilder) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Doku_Redirect/payment/dokurecurring'
            },
            redirectAfterPlaceOrder: false,
            selectPaymentMethod: function () {
                let totalAmount = window.checkoutConfig.totalsData.grand_total.toFixed(0).toString()+".00"
                window.checkoutConfig.totalsData.total = totalAmount
                $.post(url.build('payredirect/payment/checkout'), {
                    amount: totalAmount,
                    type:''
                }, function (result) {
                    $('#dokuform-redirect').attr('action','https://staging.doku.com/Suite/Receive')
                    jQuery('#dokuform-redirect').parent(".payment-method-content").show()
                    jQuery('#dokuform-redirect').parent(".payment-method-content").find(".actions-toolbar").hide()
                    Object.keys(result).forEach(k=>{
                        $('#dokuform-redirect .val').append('<input type="hidden" name="'+k+'" value="'+result[k]+'"/>')
                    })
                    console.log(result);
                })
            },
            getMailingAddress: function () {
                return window.checkoutConfig.payment.checkmo.mailingAddress;
            },
            getInstructions: function () {
                return window.checkoutConfig.payment.instructions[this.item.method];
            },
        });
    }
);
