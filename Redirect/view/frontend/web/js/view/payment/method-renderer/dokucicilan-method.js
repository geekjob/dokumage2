define(
    [
        'Magento_Checkout/js/view/payment/default',
        'jquery',
        'mage/url',
        'Magento_Checkout/js/model/url-builder',
    ],
    function (Component,$,url, urlBuilder) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Doku_Redirect/payment/dokucicilan'
            },
            redirectAfterPlaceOrder: false,
            selectPaymentMethod: function () {
                let totalAmount = window.checkoutConfig.totalsData.grand_total.toFixed(0).toString()+".00"
                window.checkoutConfig.totalsData.total = totalAmount
                $.post(url.build('payredirect/payment/checkout'), {
                    amount: totalAmount,
                    type:'ins'
                }, function (result) {
                    let insConfig = {}
                    insConfig = JSON.parse(window.checkoutConfig.payment.snap.ins)
                    let opts_bank = ""
                    insConfig.bank.forEach((i) => {

                        opts_bank = opts_bank + "<option value='" + i.code + "'>" + i.name + "</option>"
                    })
                    $('#dokuform-cicilan #cicilan-bank')[0].innerHTML = opts_bank
                    $('#dokuform-cicilan #cicilan-bank').find("option").first().attr('selected', true)
                    $("#cicilan-doku-installment-acquirer").val($('#dokuform-cicilan #cicilan-bank').find("option").first().val())
                    let offus = "SALE"
                    if (window.checkoutConfig.payment.snap.ins.off_us == "o"){
                        offus = "OFFUSINSTALLMENT"
                    }
                    $("#cicilan-doku-off-us").val(offus)
                    $("#cicilan-doku-plan-id").val(insConfig.bank[$('#dokuform-cicilan #cicilan-bank')[0].selectedIndex].plan_id)

                    const renderTenor = () => {
                        let opts_tenor = ""
                        if (insConfig.bank[$('#dokuform-cicilan #cicilan-bank')[0].selectedIndex]) {
                            insConfig.bank[$('#dokuform-cicilan #cicilan-bank')[0].selectedIndex].tenor_value.forEach((e, i) => {
                                opts_tenor = opts_tenor + "<option value='" + e + "'>" + insConfig.bank[$('#dokuform-cicilan #cicilan-bank')[0].selectedIndex].tenor_label[i] + "</option>"
                            })
                            $('#dokuform-cicilan #cicilan-tenor')[0].innerHTML = opts_tenor
                            $('#dokuform-cicilan #cicilan-tenor').find("option").first().attr('selected', true)
                            $("#cicilan-doku-tenor").val($('#dokuform-cicilan #cicilan-tenor').find("option").first().val())
                        }
                    }
                    $("#dokuform-cicilan").delegate('#cicilan-bank', 'change', function () {
                        renderTenor()
                        $("#cicilan-doku-installment-acquirer").val($('#dokuform-cicilan #cicilan-bank').val())
                        $("#cicilan-doku-plan-id").val(insConfig.bank[$('#dokuform-cicilan #cicilan-bank')[0].selectedIndex].plan_id)
                    })
                    $("#dokuform-cicilan").delegate('#cicilan-tenor', 'change', function () {
                        $("#cicilan-doku-tenor").val($('#dokuform-cicilan #cicilan-tenor').val())
                    })
                    renderTenor()
                    $('#dokuform-cicilan').attr('action','https://staging.doku.com/Suite/Receive')
                    jQuery('#dokuform-cicilan').parent(".payment-method-content").show()
                    jQuery('#dokuform-cicilan').parent(".payment-method-content").find(".actions-toolbar").hide()
                    Object.keys(result).forEach(k=>{
                        $('#dokuform-cicilan .val').append('<input type="hidden" name="'+k+'" value="'+result[k]+'"/>')
                    })
                    console.log(result);
                })
            },
            getMailingAddress: function () {
                return window.checkoutConfig.payment.checkmo.mailingAddress;
            },
            getInstructions: function () {
                return window.checkoutConfig.payment.instructions[this.item.method];
            },
        });
    }
);
