define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'dokuredirect',
                component: 'Doku_Redirect/js/view/payment/method-renderer/dokuredirect-method'
            }
        );
        return Component.extend({});
    }
);