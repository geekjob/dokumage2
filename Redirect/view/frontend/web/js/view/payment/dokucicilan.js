define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'dokucicilan',
                component: 'Doku_Redirect/js/view/payment/method-renderer/dokucicilan-method'
            }
        );
        return Component.extend({});
    }
);