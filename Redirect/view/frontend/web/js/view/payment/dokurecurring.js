define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'dokurecurring',
                component: 'Doku_Redirect/js/view/payment/method-renderer/dokurecurring-method'
            }
        );
        return Component.extend({});
    }
);