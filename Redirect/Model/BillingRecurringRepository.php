<?php


namespace Doku\Redirect\Model;

use Doku\Redirect\Api\BillingRecurringRepositoryInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Store\Model\StoreManagerInterface;
use Doku\Redirect\Api\Data\BillingRecurringInterfaceFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Doku\Redirect\Model\ResourceModel\BillingRecurring as ResourceBillingRecurring;
use Doku\Redirect\Api\Data\BillingRecurringSearchResultsInterfaceFactory;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Doku\Redirect\Model\ResourceModel\BillingRecurring\CollectionFactory as BillingRecurringCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;

class BillingRecurringRepository implements BillingRecurringRepositoryInterface
{

    protected $billingRecurringCollectionFactory;

    private $collectionProcessor;

    protected $extensibleDataObjectConverter;
    protected $dataBillingRecurringFactory;

    protected $extensionAttributesJoinProcessor;

    protected $dataObjectHelper;

    protected $resource;

    protected $billingRecurringFactory;

    private $storeManager;

    protected $dataObjectProcessor;

    protected $searchResultsFactory;


    /**
     * @param ResourceBillingRecurring $resource
     * @param BillingRecurringFactory $billingRecurringFactory
     * @param BillingRecurringInterfaceFactory $dataBillingRecurringFactory
     * @param BillingRecurringCollectionFactory $billingRecurringCollectionFactory
     * @param BillingRecurringSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceBillingRecurring $resource,
        BillingRecurringFactory $billingRecurringFactory,
        BillingRecurringInterfaceFactory $dataBillingRecurringFactory,
        BillingRecurringCollectionFactory $billingRecurringCollectionFactory,
        BillingRecurringSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->billingRecurringFactory = $billingRecurringFactory;
        $this->billingRecurringCollectionFactory = $billingRecurringCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataBillingRecurringFactory = $dataBillingRecurringFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Doku\Redirect\Api\Data\BillingRecurringInterface $billingRecurring
    ) {
        /* if (empty($billingRecurring->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $billingRecurring->setStoreId($storeId);
        } */
        
        $billingRecurringData = $this->extensibleDataObjectConverter->toNestedArray(
            $billingRecurring,
            [],
            \Doku\Redirect\Api\Data\BillingRecurringInterface::class
        );
        
        $billingRecurringModel = $this->billingRecurringFactory->create()->setData($billingRecurringData);
        
        try {
            $this->resource->save($billingRecurringModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the billingRecurring: %1',
                $exception->getMessage()
            ));
        }
        return $billingRecurringModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getById($billingRecurringId)
    {
        $billingRecurring = $this->billingRecurringFactory->create();
        $this->resource->load($billingRecurring, $billingRecurringId);
        if (!$billingRecurring->getId()) {
            throw new NoSuchEntityException(__('BillingRecurring with id "%1" does not exist.', $billingRecurringId));
        }
        return $billingRecurring->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->billingRecurringCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Doku\Redirect\Api\Data\BillingRecurringInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Doku\Redirect\Api\Data\BillingRecurringInterface $billingRecurring
    ) {
        try {
            $billingRecurringModel = $this->billingRecurringFactory->create();
            $this->resource->load($billingRecurringModel, $billingRecurring->getBillingrecurringId());
            $this->resource->delete($billingRecurringModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the BillingRecurring: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($billingRecurringId)
    {
        return $this->delete($this->getById($billingRecurringId));
    }
}
