<?php
/**
 * Doku
 * Copyright (C) 2017 lukluk
 *
 * This file is part of Doku/Snap.
 *
 * Doku/Snap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Doku\Redirect\Model\Config\Source;

class ExecuteType implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => 'DAY', 'label' => __('DAY')],
        ['value' => 'DATE', 'label' => __('DATE')],
        ['value' => 'FULLDATE', 'label' => __('FULLDATE')]];
    }

    public function toArray()
    {
        return ['DAY' => __('DAY'),
        'DATE' => __('DATE'),
        'FULLDATE' => __('FULLDATE')];
    }
}
