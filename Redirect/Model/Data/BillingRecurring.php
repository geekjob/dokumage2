<?php


namespace Doku\Redirect\Model\Data;

use Doku\Redirect\Api\Data\BillingRecurringInterface;

class BillingRecurring extends \Magento\Framework\Api\AbstractExtensibleObject implements BillingRecurringInterface
{

    /**
     * Get billingrecurring_id
     * @return string|null
     */
    public function getBillingrecurringId()
    {
        return $this->_get(self::BILLINGRECURRING_ID);
    }

    /**
     * Set billingrecurring_id
     * @param string $billingrecurringId
     * @return \Doku\Redirect\Api\Data\BillingRecurringInterface
     */
    public function setBillingrecurringId($billingrecurringId)
    {
        return $this->setData(self::BILLINGRECURRING_ID, $billingrecurringId);
    }

    /**
     * Get customerId
     * @return string|null
     */
    public function getCustomerId()
    {
        return $this->_get(self::CUSTOMERID);
    }

    /**
     * Set customerId
     * @param string $customerId
     * @return \Doku\Redirect\Api\Data\BillingRecurringInterface
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMERID, $customerId);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Doku\Redirect\Api\Data\BillingRecurringExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Doku\Redirect\Api\Data\BillingRecurringExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Doku\Redirect\Api\Data\BillingRecurringExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get orderNo
     * @return string|null
     */
    public function getOrderNo()
    {
        return $this->_get(self::ORDERNO);
    }

    /**
     * Set orderNo
     * @param string $orderNo
     * @return \Doku\Redirect\Api\Data\BillingRecurringInterface
     */
    public function setOrderNo($orderNo)
    {
        return $this->setData(self::ORDERNO, $orderNo);
    }

    /**
     * Get chargeamount
     * @return string|null
     */
    public function getChargeamount()
    {
        return $this->_get(self::CHARGEAMOUNT);
    }

    /**
     * Set chargeamount
     * @param string $chargeamount
     * @return \Doku\Redirect\Api\Data\BillingRecurringInterface
     */
    public function setChargeamount($chargeamount)
    {
        return $this->setData(self::CHARGEAMOUNT, $chargeamount);
    }
}
