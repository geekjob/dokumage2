<?php


namespace Doku\Redirect\Model\ResourceModel;

class BillingRecurring extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('doku_redirect_billingrecurring', 'billingrecurring_id');
    }
}
