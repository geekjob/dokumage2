<?php


namespace Doku\Redirect\Model\ResourceModel\BillingRecurring;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Doku\Redirect\Model\BillingRecurring::class,
            \Doku\Redirect\Model\ResourceModel\BillingRecurring::class
        );
    }
}
