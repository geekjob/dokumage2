<?php


namespace Doku\Redirect\Model;

use Doku\Redirect\Api\Data\BillingRecurringInterfaceFactory;
use Doku\Redirect\Api\Data\BillingRecurringInterface;
use Magento\Framework\Api\DataObjectHelper;

class BillingRecurring extends \Magento\Framework\Model\AbstractModel
{

    protected $billingrecurringDataFactory;

    protected $_eventPrefix = 'doku_redirect_billingrecurring';
    protected $dataObjectHelper;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param BillingRecurringInterfaceFactory $billingrecurringDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Doku\Redirect\Model\ResourceModel\BillingRecurring $resource
     * @param \Doku\Redirect\Model\ResourceModel\BillingRecurring\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        BillingRecurringInterfaceFactory $billingrecurringDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Doku\Redirect\Model\ResourceModel\BillingRecurring $resource,
        \Doku\Redirect\Model\ResourceModel\BillingRecurring\Collection $resourceCollection,
        array $data = []
    ) {
        $this->billingrecurringDataFactory = $billingrecurringDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve billingrecurring model with billingrecurring data
     * @return BillingRecurringInterface
     */
    public function getDataModel()
    {
        $billingrecurringData = $this->getData();
        
        $billingrecurringDataObject = $this->billingrecurringDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $billingrecurringDataObject,
            $billingrecurringData,
            BillingRecurringInterface::class
        );
        
        return $billingrecurringDataObject;
    }
}
