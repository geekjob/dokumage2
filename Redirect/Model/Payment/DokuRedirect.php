<?php


namespace Doku\Redirect\Model\Payment;

class DokuRedirect extends \Magento\Payment\Model\Method\AbstractMethod
{

    protected $_code = "dokuredirect";
    protected $_isOffline = true;

    public function isAvailable(
        \Magento\Quote\Api\Data\CartInterface $quote = null
    ) {
        return parent::isAvailable($quote);
    }
}
