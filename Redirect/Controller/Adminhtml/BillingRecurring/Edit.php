<?php


namespace Doku\Redirect\Controller\Adminhtml\BillingRecurring;

class Edit extends \Doku\Redirect\Controller\Adminhtml\BillingRecurring
{

    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('billingrecurring_id');
        $model = $this->_objectManager->create(\Doku\Redirect\Model\BillingRecurring::class);
        
        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Billingrecurring no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('doku_redirect_billingrecurring', $model);
        
        // 3. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Billingrecurring') : __('New Billingrecurring'),
            $id ? __('Edit Billingrecurring') : __('New Billingrecurring')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Billingrecurrings'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Edit Billingrecurring %1', $model->getId()) : __('New Billingrecurring'));
        return $resultPage;
    }
}
