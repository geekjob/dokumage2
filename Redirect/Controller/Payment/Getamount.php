<?php


namespace Doku\Redirect\Controller\Payment;

class Getamount extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $jsonHelper;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $om = $this->_objectManager;
            $bill = $om->create('Doku\Redirect\Model\ResourceModel\BillingRecurring\Collection')->addFieldToFilter('orderno', $this->getRequest()->getParam('BILLNUMBER'))->addFieldToFilter('customerid', $this->getRequest()->getParam('CUSTOMERID'));
            if($bill->getSize()){
                $data = $bill->getFirstItem();                
                return $this->jsonResponse($data->getChargeamount().".00");
            } else {
                return $this->jsonResponse("0.00");
            }        
            
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }
}
