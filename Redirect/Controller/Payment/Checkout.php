<?php
/**
 * Doku 
 * Copyright (C) 2017 lukluk 
 * 
 * This file is part of Doku/Snap.
 * 
 * Doku/Snap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Doku\Redirect\Controller\Payment;
use Magento\Framework\App\Filesystem\DirectoryList;
    $object_manager = \Magento\Framework\App\ObjectManager::getInstance();
    $filesystem = $object_manager->get('Magento\Framework\Filesystem');
    $root = $filesystem->getDirectoryRead(DirectoryList::ROOT);
    require_once($root->getAbsolutePath('app/code/Doku/lib/Doku.php'));

class Checkout extends \Magento\Framework\App\Action\Action
{
    
    protected $jsonHelper;      
    protected $checkoutSession;
    protected $iv;
    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,        
        \Doku\Snap\Model\Payment\DokuOrder $iv,
        \Magento\Framework\Json\Helper\Data $jsonHelper        
    ) {
        $this->_checkoutSession = $checkoutSession;
        $this->jsonHelper = $jsonHelper;     
        $this->iv = $iv;     
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $om = $this->_objectManager;
            $dokuInit = $om->create('Doku_Initiate');  
            $doku_Library = $om->get('doku_Library');  
            $config = $om->get('Magento\Framework\App\Config\ScopeConfigInterface');
            $mallid = $config->getValue('payment/dokusnap/merchant_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $key = $config->getValue('payment/dokusnap/client_key', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

            $dokuInit::$sharedKey=$key;
            $dokuInit::$mallId=$mallid;
            $order = $this->_checkoutSession->getQuote();
            $session = $om->get('Magento\Checkout\Model\Session');
            $checkoutData = $this->_checkoutSession->getData();            
            $orderIncrementId = str_pad(intval($checkoutData['quote_id_1']), 6, '0', STR_PAD_LEFT).str_pad(rand(0,99), 2, '0', STR_PAD_LEFT);                         
            $customerObj = $om->create('Magento\Customer\Model\Customer')
            ->load($order->getCustomer()->getId());        
            $cust = $customerObj->getData();
            $customerName = $cust['firstname']." ".$cust["lastname"];

            //$orderIncrementId = rand(0,1000);
            
            $data = $this->getRequest()->getPost();   
            $type = $data['type'];         
            if (!$data['amount']){
                return $this->jsonResponse(['error' => true,'data' => $data]);
            }            
            $params = array(
                'amount' => $data['amount'],
                'billno' => $orderIncrementId, 'currency' => '360',
                'customerid' => $order->getCustomer()->getId()
            );
            
            $basket = $doku_Library::formatBasket($this->iv->getBasket($order, $data['amount']));
            $checkout = [];
            $checkout['MALLID'] = $mallid;
            $checkout['CHAINMERCHANT'] = "NA";
            $checkout['AMOUNT'] = $data['amount'];
            $checkout['PURCHASEAMOUNT'] = $data['amount'];
            $checkout['TRANSIDMERCHANT'] = $orderIncrementId;
            $checkout['WORDS'] = $doku_Library::doCreateWords($params);
            $checkout['REQUESTDATETIME'] = date('YmdHis');            
            $checkout['CURRENCY'] =  360;
            $checkout['PURCHASECURRENCY'] = 360;
            $checkout['SESSIONID'] = $orderIncrementId;
            $checkout['NAME'] = $customerName;
            $checkout['EMAIL'] = $customerObj->getEmail();
            $checkout['BASKET'] = $basket;
            if($type=="recurring"){
                $checkout['CUSTOMERID'] = $order->getCustomer()->getId();
                $checkout['PAYMENTCHANNEL'] = 17;
                $checkout['BILLNUMBER'] = $orderIncrementId;
                $checkout['BILLDETAIL'] = $basket;                                
                $checkout['EXECUTETYPE'] = $config->getValue('payment/dokurecurring/executetype', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $checkout['EXECUTEDATE'] = $config->getValue('payment/dokurecurring/executedate', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $checkout['EXECUTEMONTH'] = $config->getValue('payment/dokurecurring/executemonth', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $checkout['STARTDATE'] = date("Ymd");
                $checkout['ENDDATE']="NA";
                $isFlat = "FALSE";
                if ($config->getValue('payment/dokurecurring/flatstatus', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)==1){
                    $isFlat = "TRUE";
                }
                $checkout['FLATSTATUS'] = $isFlat;
                if ($checkout['FLATSTATUS']=="Y")
                {
                    $checkout['REGISTERAMOUNT'] = $checkout["AMOUNT"];
                } else {
                    $checkout['REGISTERAMOUNT']="10000.00";
                }
                
            } else 
            if ($type=="ins"){                
                $checkout['PAYMENTCHANNEL']=15;                                
            }
            
            return $this->jsonResponse($checkout);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {            
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }
}
