<?php


namespace Doku\Redirect\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $table_doku_redirect_billingrecurring = $setup->getConnection()->newTable($setup->getTable('doku_redirect_billingrecurring'));

        $table_doku_redirect_billingrecurring->addColumn(
            'billingrecurring_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,],
            'Entity ID'
        );

        $table_doku_redirect_billingrecurring->addColumn(
            'customerId',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'customerId'
        );

        $table_doku_redirect_billingrecurring->addColumn(
            'orderNo',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'orderNo'
        );

        $table_doku_redirect_billingrecurring->addColumn(
            'chargeamount',
            \Magento\Framework\DB\Ddl\Table::TYPE_NUMERIC,
            null,
            [],
            'chargeamount'
        );

        $setup->getConnection()->createTable($table_doku_redirect_billingrecurring);
    }
}
