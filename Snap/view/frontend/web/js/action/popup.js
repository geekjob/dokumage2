define(
    [
        'jquery',
        'mage/url',
        'Magento_Checkout/js/model/url-builder',
        'Doku_Snap/js/view/payment/form',
        'Doku_Snap/js/view/payment/list',
        'Doku_Snap/js/view/payment/event',
    ],
    function ($, url, urlBuilder, paymentForm, paymentList,eventPopup) {
        'use strict';

        return function () {
            let totalAmount = window.checkoutConfig.totalsData.grand_total.toFixed(0).toString()+".00"
            window.checkoutConfig.totalsData.total = totalAmount
            const renderPaymentList = ()=>{
                $.post(url.build('pay/payment/genword'), {
                    amount: totalAmount,
                }, function (result) {
                    console.log("payment list")
                    
                    paymentList()
                    eventPopup(result)
                    $('.doku-form').hide()
                    $('.doku-payment').show()
                    $('#payment-body').show();

                })  
            }           
            
            $(".back-icon").click(function () {                                
                renderPaymentList()                                
            })

            renderPaymentList()
        };
    }
);