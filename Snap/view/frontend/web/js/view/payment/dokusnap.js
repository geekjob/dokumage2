define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'dokusnap',
                component: 'Doku_Snap/js/view/payment/method-renderer/dokusnap-method'
            }
        );
        return Component.extend({});
    }
);