define(
    [
        'Magento_Checkout/js/view/payment/default',
        'Doku_Snap/js/action/popup',
        'Doku_Snap/js/view/helper/loadjs',
        'Doku_Snap/js/view/helper/loadcss',
        'jquery',
        'mage/url',
        'Magento_Checkout/js/model/url-builder',
    ],
    function (Component, popup, loadJs,loadCss, $, url, urlBuilder) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Doku_Snap/payment/dokusnap'
            },
            redirectAfterPlaceOrder: false,
            placeOrder: function () {
                var client_key = window.checkoutConfig.payment.snap.clientkey;
                var merchant_id = window.checkoutConfig.payment.snap.merchantid;
                console.log('client_key = ' + client_key);
                console.log('merchant_id = ' + merchant_id);
                //$('#payment-form').attr('action', window.checkoutConfig.payment.snap.pay_url + "pay/payment/index")
                
                window.exports = {}
                window.env = (checkoutConfig.payment.snap.production!="1") ? "staging" : "production"
                loadJs("https://staging.doku.com/doku-js/assets/js/doku.uncompress-1.5-SNAPSHOT.js")
                .then(()=>{
                    return loadJs("https://cdn.jsdelivr.net/npm/ua-parser-js@0.7.19/src/ua-parser.min.js")
                })
                .then(popup)                
            },
            getMailingAddress: function () {
                return window.checkoutConfig.payment.checkmo.mailingAddress;
            },
            getInstructions: function () {
                return window.checkoutConfig.payment.instructions[this.item.method];
            },
        });
    }
);