define(
    [
        'jquery',
    ],
    function ($) {
        'use strict';

        return function (id) {           
            let form = {}
            form.doku = `<form action="http://localhost:3000/" method="POST" id="payment-form"> <div doku-div='form-payment'>
            <input id="doku-token" name="doku-token" type="hidden" />
            <input id=" doku-pairing-code" name="doku-pairing-code" type="hidden" /> </div>
            </form>
        </div>`
            document.querySelector("#doku-form").innerHTML = form[id];
        };
    }
);