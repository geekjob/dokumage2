
define(
    [                
    ],
    function () {
        'use strict';

        return function () {
            let item = `<div class="payment">
          <div class="payment-image"><span class="pay-icons ID-icon" alt="NAME"></span></div>          
          <input class="payment-option" type="radio" name="ID" id="payment-ID"/>
          <label class="payment-label is-ID" for="payment-ID">NAME</label>          
          <label class="payment-label2 is-ID" for="payment-ID">DESC</label>          
        </div>`;
            let map_channels = {}
            map_channels['cc'] = {
                name: "Credit Card",
                desc: "Pay with Visa, MasterCard, or JCB",
                x: "0",
                y: "-23",
            }

            map_channels['ccins'] = {
                name: "Credit Card Installments",
                desc: "Cicilan",
                x: "0",
                y: "-23",
            }

            map_channels['wallet'] = {
                name: "Doku Wallet",
                desc: "Use your wallet",
                x: "0",
                y: "0"
            }

            map_channels['offline'] = {
                name: "Bayar di counter",
                desc: "indomart/alfamart/lawson dan yang lain",
                x: "0",
                y: "0"
            }

            map_channels['va'] = {
                name: "Virtual Account",
                desc: "Bank Transfer, Convenience Store",
                x: "0",
                y: "0"
            }
            map_channels['ib'] = {
                name: "Internet Banking",
                desc: "Mandiri Internet Banking",
                x: "0",
                y: "0"
            }
            let htmlx = ""
            let channels = window.checkoutConfig.payment.snap.channels.split(',')
            for (var i in channels) {
                let payment = channels[i]
                let htmlItem = ""
                htmlItem = item.replace(/NAME/g, map_channels[payment].name)
                htmlItem = htmlItem.replace(/DESC/g, map_channels[payment].desc)
                htmlItem = htmlItem.replace(/XX/g, map_channels[payment].x)
                htmlItem = htmlItem.replace(/YY/g, map_channels[payment].x)
                htmlItem = htmlItem.replace(/ID/g, payment)
                htmlx = htmlx + htmlItem
            }
            document.querySelector("#payment-body").innerHTML = htmlx;
            
        };
    }
);
