define(
    [
        'jquery',
        'mage/url',
        'Magento_Checkout/js/model/url-builder',
    ],
    function ($, url, urlBuilder) {
        'use strict';

        return function (result) {
            function moment(unix){
                const hari = ["Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu"]
                const bulan = ['Januari',
				'Februari',
				'Maret',
				'April',
				'Mei',
				'Juni',
				'Juli',
				'Agustus',
				'September',
				'Oktober',
				'November',
				'Desember']
                let d = new Date(unix)
                let HH = hari[d.getDay()]
                let MM = bulan[d.getMonth()]
                return HH+", "+d.getDate()+" "+MM+" "+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()
            }
            $("#payment-body").delegate('#payment-cc', 'click', function () {
                $('#payment-body').hide()
                $('#doku-form').show()
                var data = new Object();
                data.req_merchant_code = window.checkoutConfig.payment.snap.merchantid;
                data.req_chain_merchant = 'NA';
                data.req_payment_channel = '15';
                data.req_transaction_id = result.invoice ? result.invoice : 1;
                data.req_currency = 360;
                data.req_amount = window.checkoutConfig.totalsData.total;
                data.req_words = result.words;
                data.req_customer_id = window.checkoutConfig.customerData.id;
                data.req_form_type = 'full';
                data.req_server_url = url.build('pay/payment/cc')
                getForm(data, window.env)
            })
            $("#payment-body").delegate('#payment-wallet', 'click', function () {
                $('#payment-body').hide()
                $('#doku-form').show()
                var data = new Object();
                data.req_merchant_code = window.checkoutConfig.payment.snap.merchantid;
                data.req_chain_merchant = 'NA';
                data.req_payment_channel = '04';
                data.req_transaction_id = result.invoice ? result.invoice : 1;
                data.req_currency = 360;
                data.req_amount = window.checkoutConfig.totalsData.total;
                data.req_words = result.words;
                data.req_customer_id = window.checkoutConfig.customerData.id;
                data.req_form_type = 'full';
                data.req_server_url = url.build('pay/payment/wallet')
                getForm(data, window.env)
            })
            $("#payment-body").delegate('#payment-offline', 'click', function () {
                $('#payment-body').show()
                $('.doku-form').hide()
                $("#payment-body").html($('#loading-form')[0].innerHTML)
                $.post(url.build('pay/payment/offline'), {
                    doku_invoice_no: result.invoice,
                    doku_amount: window.checkoutConfig.totalsData.total,
                }, function (res) {
                    if (res['res_response_code'] != '0000') {
                        $("#payment-body").html($('#error-form')[0].innerHTML)
                        $('#message').html(res['res_response_msg'])
                        $(".back-button").click(function () {
                            $(".back-icon").click()
                        })
                        return false
                    }
                    $('.back-nav-pay').hide()
                    $("#payment-body").html($('#offline-form')[0].innerHTML)
                    startTimer(res.trxdate,window.checkoutConfig.payment.snap.convenience_timeout, "timmer")                    
                    $('#codealfamart').html('88888' + res.res_pay_code)
                    $('#codeindomart').html('88220' + res.res_pay_code)
                    var trxdate = moment(res.trxdate);
                    $('#timeoutText').html(trxdate)
                    $('#total').val(window.checkoutConfig.totalsData.total)

                })
            })
            $("#payment-body").delegate('#payment-va', 'click', function () {
                $('#payment-body').show()
                $('.doku-form').hide()
                $("#payment-body").html($('#loading-form')[0].innerHTML)
                $.post(url.build('pay/payment/offline'), {
                    doku_invoice_no: result.invoice,
                    doku_amount: window.checkoutConfig.totalsData.total,
                }, function (res) {
                    if (res['res_response_code'] != '0000') {
                        $("#payment-body").html($('#error-form')[0].innerHTML)
                        $('#message').html(res['res_response_msg'])
                        $(".back-button").click(function () {
                            $(".back-icon").click()
                        })
                        return false
                    }
                    $('.back-nav-pay').hide()
                    $("#payment-body").html($('#va-form')[0].innerHTML)
                    startTimer(res.trxdate,window.checkoutConfig.payment.snap.convenience_timeout, "timmer")
                    $('#code').html('89650' + res.res_pay_code)
                    var trxdate = moment(res.trxdate);
                    $('#timeoutText').html(trxdate)
                    $('#total').val(window.checkoutConfig.totalsData.total)

                })
            })
            $("#payment-body").delegate('#payment-ib', 'click', function () {
                //$("#payment-body").html($('#ib-form')[0].innerHTML)
                $('#payment-body').hide()
                $('.doku-form').hide()
                $('#ib-form').show()
                $('.cc-number').payment('formatCardNumber');
                var challenge3 = Math.floor(Math.random() * 999999999);
                $("#CHALLENGE_CODE_3").val(challenge3);
                var data = new Object();
                data.req_cc_field = 'cc_number';
                data.req_challenge_field = 'CHALLENGE_CODE_1';
                dokuMandiriInitiate(data);
            })
            $("#payment-body").delegate('#payment-ccins', 'click', function () {
                $("#payment-body").html($('#ins-form')[0].innerHTML)
                $('#payment-body').show()
                $('.doku-form').hide()
                let insConfig = {}

                insConfig.bank = []
                try {
                    insConfig = JSON.parse(window.checkoutConfig.payment.snap.ins)
                    let opts_bank = ""
                    insConfig.bank.forEach((i) => {

                        opts_bank = opts_bank + "<option value='" + i.code + "'>" + i.name + "</option>"
                    })
                    $('#payment-body #bank')[0].innerHTML = opts_bank
                    $('#payment-body #bank').find("option").first().attr('selected', true)
                    $("#doku-installment-acquirer").val($('#payment-body #bank').find("option").first().val())

                    $("#doku-off-us").val(window.checkoutConfig.payment.snap.ins.off_us)
                    $("#doku-plan-id").val(insConfig.bank[$('#payment-body #bank')[0].selectedIndex].plan_id)

                    const renderTenor = () => {
                        let opts_tenor = ""
                        if (insConfig.bank[$('#payment-body #bank')[0].selectedIndex]) {
                            insConfig.bank[$('#payment-body #bank')[0].selectedIndex].tenor_value.forEach((e, i) => {
                                opts_tenor = opts_tenor + "<option value='" + e + "'>" + insConfig.bank[$('#payment-body #bank')[0].selectedIndex].tenor_label[i] + "</option>"
                            })
                            $('#payment-body #tenor')[0].innerHTML = opts_tenor
                            $('#payment-body #tenor').find("option").first().attr('selected', true)
                            $("#doku-tenor").val($('#payment-body #tenor').find("option").first().val())
                        }
                    }
                    $("#payment-body").delegate('#bank', 'change', function () {
                        renderTenor()
                        $("#doku-installment-acquirer").val($('#payment-body #bank').val())
                        $("#doku-plan-id").val(insConfig.bank[$('#payment-body #bank')[0].selectedIndex].plan_id)
                    })
                    $("#payment-body").delegate('#tenor', 'change', function () {
                        $("#doku-tenor").val($('#payment-body #tenor').val())
                    })
                    renderTenor()

                } catch (e) {
                    alert(e)
                }
                $("#payment-body #insnext").click(function () {
                    // let buildSession = {}
                    // buildSession.tenor = $('#payment-body #tenor').val()
                    // buildSession.bank = $('#payment-body #bank').val()
                    // buildSession.plan_id = insConfig.bank[$('#payment-body #bank')[0].selectedIndex].plan_id
                    // buildSession.off_us = insConfig.off_us

                    // // $.post(url.build('pay/payment/session'), {
                    // //     session: JSON.stringify(buildSession),
                    // // }, function (result) {})

                    $('#payment-body').hide()
                    $('#doku-form').show()
                    var data = new Object();
                    data.req_merchant_code = window.checkoutConfig.payment.snap.merchantid;
                    data.req_chain_merchant = 'NA';
                    data.req_payment_channel = '15';
                    data.req_transaction_id = result.invoice ? result.invoice : 1;
                    data.req_currency = 360;
                    data.req_amount = window.checkoutConfig.totalsData.total;
                    data.req_words = result.words;
                    data.req_form_type = 'full';
                    data.req_server_url = url.build('pay/payment/cc')
                    getForm(data, window.env)

                })
            })
        };
    }
);