define(
    [
        'jquery',
    ],
    function ($) {
        'use strict';

        return function (js) {
            return new Promise(resolve => {
                $.ajax({
                    url: js,
                    dataType: "script",
                    success: function () {
                        resolve()
                    }
                })
            })

        };
    }
);