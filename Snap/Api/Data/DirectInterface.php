<?php
/**
 * Doku 
 * Copyright (C) 2017 lukluk 
 * 
 * This file is part of Doku/Snap.
 * 
 * Doku/Snap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Doku\Snap\Api\Data;

interface DirectInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const DIRECT_ID = 'direct_id';
    const TOKEN = 'token';
    const CUSTOMER_ID = 'customer_id';
    const ORDER_ID = 'order_id';

    /**
     * Get direct_id
     * @return string|null
     */
    public function getDirectId();

    /**
     * Set direct_id
     * @param string $directId
     * @return \Doku\Snap\Api\Data\DirectInterface
     */
    public function setDirectId($directId);

    /**
     * Get token
     * @return string|null
     */
    public function getToken();

    /**
     * Set token
     * @param string $token
     * @return \Doku\Snap\Api\Data\DirectInterface
     */
    public function setToken($token);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Doku\Snap\Api\Data\DirectExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Doku\Snap\Api\Data\DirectExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Doku\Snap\Api\Data\DirectExtensionInterface $extensionAttributes
    );

    /**
     * Get customer_id
     * @return string|null
     */
    public function getCustomerId();

    /**
     * Set customer_id
     * @param string $customerId
     * @return \Doku\Snap\Api\Data\DirectInterface
     */
    public function setCustomerId($customerId);

    /**
     * Get order_id
     * @return string|null
     */
    public function getOrderId();

    /**
     * Set order_id
     * @param string $orderId
     * @return \Doku\Snap\Api\Data\DirectInterface
     */
    public function setOrderId($orderId);
}