<?php
/**
 * Doku 
 * Copyright (C) 2017 lukluk 
 * 
 * This file is part of Doku/Snap.
 * 
 * Doku/Snap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Doku\Snap\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface DirectRepositoryInterface
{

    /**
     * Save direct
     * @param \Doku\Snap\Api\Data\DirectInterface $direct
     * @return \Doku\Snap\Api\Data\DirectInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Doku\Snap\Api\Data\DirectInterface $direct
    );

    /**
     * Retrieve direct
     * @param string $directId
     * @return \Doku\Snap\Api\Data\DirectInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($directId);

    /**
     * Retrieve direct matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Doku\Snap\Api\Data\DirectSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete direct
     * @param \Doku\Snap\Api\Data\DirectInterface $direct
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Doku\Snap\Api\Data\DirectInterface $direct
    );

    /**
     * Delete direct by ID
     * @param string $directId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($directId);
}