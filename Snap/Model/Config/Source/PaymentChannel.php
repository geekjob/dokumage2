<?php
/**
 * Doku
 * Copyright (C) 2017 lukluk
 *
 * This file is part of Doku/Snap.
 *
 * Doku/Snap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Doku\Snap\Model\Config\Source;

class PaymentChannel implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => 'cc', 'label' => __('Credit Card')],['value' => 'ccins', 'label' => __('Credit Card Installments')],['value' => 'wallet', 'label' => __('Doku Wallet')],['value' => 'va', 'label' => __('Virtual Account/Bank Transfer')],['value' => 'offline', 'label' => __('Convenience Store')],['value' => 'ib', 'label' => __('Internet Banking')]];
    }

    public function toArray()
    {
        return ['cc' => __('cc'),'ccins' => __('ccins'),'wallet' => __('wallet'),'va' => __('va'),'offline' => __('offline'),'ib' => __('ib')];
    }
}
