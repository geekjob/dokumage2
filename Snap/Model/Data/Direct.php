<?php
/**
 * Doku 
 * Copyright (C) 2017 lukluk 
 * 
 * This file is part of Doku/Snap.
 * 
 * Doku/Snap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Doku\Snap\Model\Data;

use Doku\Snap\Api\Data\DirectInterface;

class Direct extends \Magento\Framework\Api\AbstractExtensibleObject implements DirectInterface
{

    /**
     * Get direct_id
     * @return string|null
     */
    public function getDirectId()
    {
        return $this->_get(self::DIRECT_ID);
    }

    /**
     * Set direct_id
     * @param string $directId
     * @return \Doku\Snap\Api\Data\DirectInterface
     */
    public function setDirectId($directId)
    {
        return $this->setData(self::DIRECT_ID, $directId);
    }

    /**
     * Get token
     * @return string|null
     */
    public function getToken()
    {
        return $this->_get(self::TOKEN);
    }

    /**
     * Set token
     * @param string $token
     * @return \Doku\Snap\Api\Data\DirectInterface
     */
    public function setToken($token)
    {
        return $this->setData(self::TOKEN, $token);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Doku\Snap\Api\Data\DirectExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Doku\Snap\Api\Data\DirectExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Doku\Snap\Api\Data\DirectExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get customer_id
     * @return string|null
     */
    public function getCustomerId()
    {
        return $this->_get(self::CUSTOMER_ID);
    }

    /**
     * Set customer_id
     * @param string $customerId
     * @return \Doku\Snap\Api\Data\DirectInterface
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * Get order_id
     * @return string|null
     */
    public function getOrderId()
    {
        return $this->_get(self::ORDER_ID);
    }

    /**
     * Set order_id
     * @param string $orderId
     * @return \Doku\Snap\Api\Data\DirectInterface
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }
}