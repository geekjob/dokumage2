<?php
/**
 * Doku 
 * Copyright (C) 2017 lukluk 
 * 
 * This file is part of Doku/Snap.
 * 
 * Doku/Snap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Doku\Snap\Model;

use Doku\Snap\Api\Data\DirectInterface;
use Doku\Snap\Api\Data\DirectInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Direct extends \Magento\Framework\Model\AbstractModel
{

    protected $_eventPrefix = 'doku_snap_direct';
    protected $directDataFactory;

    protected $dataObjectHelper;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param DirectInterfaceFactory $directDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Doku\Snap\Model\ResourceModel\Direct $resource
     * @param \Doku\Snap\Model\ResourceModel\Direct\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        DirectInterfaceFactory $directDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Doku\Snap\Model\ResourceModel\Direct $resource,
        \Doku\Snap\Model\ResourceModel\Direct\Collection $resourceCollection,
        array $data = []
    ) {
        $this->directDataFactory = $directDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve direct model with direct data
     * @return DirectInterface
     */
    public function getDataModel()
    {
        $directData = $this->getData();
        
        $directDataObject = $this->directDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $directDataObject,
            $directData,
            DirectInterface::class
        );
        
        return $directDataObject;
    }
}