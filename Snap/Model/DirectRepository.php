<?php
/**
 * Doku 
 * Copyright (C) 2017 lukluk 
 * 
 * This file is part of Doku/Snap.
 * 
 * Doku/Snap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Doku\Snap\Model;

use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Doku\Snap\Api\Data\DirectSearchResultsInterfaceFactory;
use Doku\Snap\Api\DirectRepositoryInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Doku\Snap\Model\ResourceModel\Direct\CollectionFactory as DirectCollectionFactory;
use Doku\Snap\Api\Data\DirectInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Store\Model\StoreManagerInterface;
use Doku\Snap\Model\ResourceModel\Direct as ResourceDirect;

class DirectRepository implements DirectRepositoryInterface
{

    protected $resource;

    protected $extensionAttributesJoinProcessor;

    protected $dataObjectProcessor;

    protected $extensibleDataObjectConverter;
    private $storeManager;

    protected $dataObjectHelper;

    protected $directFactory;

    private $collectionProcessor;

    protected $searchResultsFactory;

    protected $directCollectionFactory;

    protected $dataDirectFactory;


    /**
     * @param ResourceDirect $resource
     * @param DirectFactory $directFactory
     * @param DirectInterfaceFactory $dataDirectFactory
     * @param DirectCollectionFactory $directCollectionFactory
     * @param DirectSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceDirect $resource,
        DirectFactory $directFactory,
        DirectInterfaceFactory $dataDirectFactory,
        DirectCollectionFactory $directCollectionFactory,
        DirectSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->directFactory = $directFactory;
        $this->directCollectionFactory = $directCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataDirectFactory = $dataDirectFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Doku\Snap\Api\Data\DirectInterface $direct
    ) {
        /* if (empty($direct->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $direct->setStoreId($storeId);
        } */
        
        $directData = $this->extensibleDataObjectConverter->toNestedArray(
            $direct,
            [],
            \Doku\Snap\Api\Data\DirectInterface::class
        );
        
        $directModel = $this->directFactory->create()->setData($directData);
        
        try {
            $this->resource->save($directModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the direct: %1',
                $exception->getMessage()
            ));
        }
        return $directModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getById($directId)
    {
        $direct = $this->directFactory->create();
        $this->resource->load($direct, $directId);
        if (!$direct->getId()) {
            throw new NoSuchEntityException(__('direct with id "%1" does not exist.', $directId));
        }
        return $direct->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->directCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Doku\Snap\Api\Data\DirectInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Doku\Snap\Api\Data\DirectInterface $direct
    ) {
        try {
            $directModel = $this->directFactory->create();
            $this->resource->load($directModel, $direct->getDirectId());
            $this->resource->delete($directModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the direct: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($directId)
    {
        return $this->delete($this->getById($directId));
    }
}