<?php
namespace Doku\Snap\Model\Payment;

use Magento\Quote\Api\Data\CartInterface;
use Magento\Payment\Model\Method\AbstractMethod;
use Doku\Snap\Model\Config\Source\Order\Status\Paymentreview;
use Magento\Sales\Model\Order;

class DokuSnap extends \Magento\Payment\Model\Method\AbstractMethod
{
    const SNAP_PAYMENT_CODE = 'dokusnap';
    /**
     * @var bool
     */
    protected $_isInitializeNeeded = true;

    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'dokusnap';

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = true;

    /**
     * Sidebar payment info block
     *
     * @var string
     */
//    protected $_infoBlockType = 'Magento\Payment\Block\Info\Instructions';

    protected $_isProduction;

    protected $orderFactory;


    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []){
        $this->orderFactory = $orderFactory;
        parent::__construct($context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data);
    }


    //@param \Magento\Framework\Object|\Magento\Payment\Model\InfoInterface $payment
    public function getAmount($orderId)//\Magento\Framework\Object $payment)
    {   //\Magento\Sales\Model\OrderFactory
        $orderFactory=$this->orderFactory;
        /** @var \Magento\Sales\Model\Order $order */
        // $order = $payment->getOrder();
        // $order->getIncrementId();
        /* @var $order \Magento\Sales\Model\Order */

            $order = $orderFactory->create()->loadByIncrementId($orderId);
            //$payment= $order->getPayment();

        // return $payment->getAmount();
        return $order->getGrandTotal();
    }

    protected function getOrder($orderId)
    {
        $orderFactory=$this->orderFactory;
        return $orderFactory->create()->loadByIncrementId($orderId);

    }

    /**
     * Set order state and status
     *
     * @param string $paymentAction
     * @param \Magento\Framework\Object $stateObject
     * @return void
     */
    public function initialize($paymentAction, $stateObject)
    {
        $state = $this->getConfigData('order_status');
        $this->_isProduction=$this->getConfigData('is_production');
        $stateObject->setStatus($state);
        $stateObject->setIsNotified(false);
    }

    /**
     * Check whether payment method can be used
     *
     * @param CartInterface|null $quote
     * @return bool
     */
    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null)
    {
        if ($quote === null) {
            return false;
        }
//        $isAllowedShipping = $this->isCarrierAllowed($quote->getShippingAddress()->getShippingMethod();
//        return parent::isAvailable($quote) && $isAllowedShipping;
        return parent::isAvailable($quote);
    }    

    public function getPostData($orderId)
    {   //TODO: add curency
        //OutSumCurrency
        $PostData=[];
        $PostData['OutSum']=round($this->getAmount($orderId), 2);
        $PostData['InvId']=intval($orderId);
        $PostData['MerchantLogin']=$this->getConfigData('merchant_id');
        $PostData['Description']="payment for order ".$orderId;        
        return $PostData;

    }

    public function process($responseData)
    {
        $debugData = ['response' => $responseData];
        $this->_debug($debugData);
        file_put_contents("/tmp/response.txt",$responseData);

        // $this->mapGatewayResponse($responseData, $this->getResponse());
        if(count($responseData)>2){
         $order = $this->getOrder(sprintf("%010s",$responseData['InvId']));

        if ($order) {
            echo $this->_processOrder($order,$responseData);
        }
        }else{
            echo "errors";
        }
    }

    protected function _processOrder(\Magento\Sales\Model\Order $order , $response)
    {
        //$response = $this->getResponse();
        $payment = $order->getPayment();
        //$payment->setTransactionId($response->getPnref())->setIsTransactionClosed(0);
        //TODO: add validation for request data

         try {
            $errors = array();
            $payment->setTransactionId($response["InvId"])->setIsTransactionClosed(0);
                $order->setStatus(Order::STATE_PENDING_PAYMENT);
                $order->save();
                return "Ok".$response["InvId"];             
        } catch (Exception $e) {
            return array("Internal error:" . $e->getMessage());
        }

          
    }

    public function getOrderPlaceRedirectUrl(){
        return 'http://www.google.com/';
    }   

}
