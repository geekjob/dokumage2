<?php
/**
 * Doku
 * Copyright (C) 2017 lukluk
 *
 * This file is part of Doku/Snap.
 *
 * Doku/Snap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Doku\Snap\Model\Payment;

use Magento\Sales\Model\Order;

class DokuOrder extends \Magento\Framework\App\Helper\AbstractHelper
{

    protected $quoteFactory;
    protected $paymentFactory;
    protected $_storeManager;
    protected $_product;
    protected $invoice;
    protected $_formkey;
    protected $quote;
    protected $quoteManagement;
    protected $customerFactory;
    protected $_orderRepository;
    protected $customerRepository;
    protected $_invoiceService;
    protected $_transaction;
    protected $orderService;

    /**
     * Define resource model
     *
     * @return void
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Model\Service\InvoiceService $invoiceService,
        \Magento\Framework\DB\Transaction $transaction,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product $product,
        \Magento\Framework\Data\Form\FormKey $formkey,
        \Magento\Quote\Model\QuoteManagement $quoteManagement,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Sales\Model\Service\OrderService $orderService,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Quote\Model\Quote\PaymentFactory $paymentFactory,
        \Magento\Sales\Model\Order\Invoice $invoice
    ) {
        $this->_storeManager = $storeManager;
        $this->_product = $product;
        $this->_formkey = $formkey;
        $this->quoteManagement = $quoteManagement;
        $this->customerFactory = $customerFactory;
        $this->quoteFactory = $quoteFactory;
        $this->customerRepository = $customerRepository;
        $this->orderService = $orderService;
        $this->paymentFactory = $paymentFactory;
        $this->invoice = $invoice;
        $this->_orderRepository = $orderRepository;
        $this->_invoiceService = $invoiceService;
        $this->_transaction = $transaction;
        parent::__construct($context);
    }
    public function getCustomer($order)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerObj = $objectManager->create('Magento\Customer\Model\Customer')
            ->load($order->getCustomer()->getId());
        $addr = $order->getShippingAddress()->getData();
        $cust = $customerObj->getData();
        $customer = array(
            'id' => ($order->getCustomer()->getId() ? $order->getCustomer()->getId() : "111" . rand(0, 99)), 'name' => $cust['firstname'] . ' ' . $cust['lastname'],
            'data_phone' => $order->getShippingAddress()->getTelephone()?$order->getShippingAddress()->getTelephone():"0998765444454", 'data_email' => ($order->getCustomerEmail() ? $order->getCustomerEmail() : "guest@guest.com"), 'data_address' => $addr['city']?$addr['city']:"none",
        );        
        return $customer;
    }
    public function getBasket($order, $total)
    {
        $basket = [];
        $orderItems = $order->getAllVisibleItems();
        $totPrice = 0;
        foreach ($orderItems as $item) {
            $basket[] = array(
                'name' => $item->getName(), 'amount' => number_format($item->getPrice(), 0) . ".00", 'quantity' => number_format($item->getQty(), 0), 'subtotal' => number_format(intval($item->getPrice()) * intval($item->getQty()), 0) . ".00",
            );
            $totPrice += intval($item->getPrice()) * intval($item->getQty());
        }
        $totPrice = intval($total) - $totPrice;
        if ($totPrice != "0.00") {
            $basket[] = array(
                'name' => "additional", 'amount' => number_format($totPrice, 0) . ".00", 'quantity' => '1', 'subtotal' => number_format($totPrice, 2),
            );
        }
        return $basket;
    }

    public function getUrl($url)
    {
        $storeId = $this->_storeManager->getDefaultStoreView()->getStoreId();
        return $this->_storeManager->getStore($storeId)->getUrl($url);
    }

    public function updateInvoice($invoiceId,$success){
        $invoice = $this->invoice->loadByIncrementId($invoiceId);
        $order = $invoice->getOrder();

    }

    public function createInvoice($orderId)
    {        
        $order = $this->_orderRepository->get($orderId);
        if ($order->canInvoice()) {
            $invoice = $this->_invoiceService->prepareInvoice($order);
            $invoice->register();
            $invoice->save();
            $transactionSave = $this->_transaction->addObject(
                $invoice
            )->addObject(
                $invoice->getOrder()
            );
            $transactionSave->save();            
            //send notification code
            $order->addStatusHistoryComment(
                __('Notified customer about invoice #%1.', $invoice->getId())
            )
                ->setIsCustomerNotified(true)
                ->save();
            return $invoice->getIncrementId();
        }
        return false;
    }

    public function createTransaction($ordernumber, $transaction_id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    
        $order = $objectManager->create('\Magento\Sales\Model\Order')->load($ordernumber);
        $payment = $order->getPayment();

        $trans  = $objectManager->create('Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface'); 
        $transaction = $trans->setPayment($payment)->setOrder($order)->setTransactionId($transaction_id);

        $payment->addTransactionCommentsToOrder($transaction,$transaction_id );
        $payment->save();
    }
    
    public function createOrder($quoteId, $invoiceId, $success)
    {

        //Set Address to quote

        $quote = $this->quoteFactory->create()->loadByIdWithoutStore($quoteId);
        
        // Create Order From Quote
        $quote->getPayment()->setMethod('dokusnap');
        $quote->getPayment()->setTransactionId($invoiceId)->setIsTransactionClosed(0);

        $order = $this->quoteManagement->submit($quote);

        $order->setEmailSent(1);        
        //$response = $this->getResponse();
        
        //TODO: add validation for request data

        try {
            $errors = array();
            if ($success == "paid") {
                $order->setStatus(Order::STATE_COMPLETE);
            } else
            if ($success == "pending") {
                $order->setStatus(Order::STATE_PENDING_PAYMENT);
            } else
            {
                $order->setStatus(Order::STATE_CANCELED);
            }
            $order->save();
            $inv = $this->createInvoice($order->getId());   
            
            return $inv;            
        } catch (Exception $e) {
            return false;
        }        
    }
}
