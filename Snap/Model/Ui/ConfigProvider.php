<?php
namespace Doku\Snap\Model\Ui;

use Magento\Checkout\Model\ConfigProviderInterface;
use \Doku\Snap\Model\Payment\DokuSnap;
use Magento\Framework\UrlInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Asset\Repository;
use Psr\Log\LoggerInterface;
use Magento\Payment\Model\Config as PaymentConfig; 
use Magento\Quote\Model\Quote;
use Magento\Checkout\Model\Session;

final class ConfigProvider  implements ConfigProviderInterface{
    const CODE = 'snap';
    protected $iv;
    protected $config;
    protected $request;
    protected $assetRepo;
    protected $logger;
    protected $urlBuilder;
    protected $paymentHelper;
    protected $methods = [];
    protected $_checkoutSession;


    public function __construct(
        PaymentConfig $paymentConfig, 
        Repository $assetRepo,
        RequestInterface $request,
        UrlInterface $urlBuilder,
        \Doku\Snap\Model\Payment\DokuSnap $iv,
        \Magento\Checkout\Model\Session $checkoutSession,
        LoggerInterface $logger){

        $this->urlBuilder = $urlBuilder;
        $this->logger = $logger;
        $this->iv = $iv;
        $this->config = $paymentConfig;
        $this->assetRepo = $assetRepo; 
        $this->_checkoutSession = $checkoutSession;
    }

    public function getConfig()
    {
        $production = $this->iv->getConfigData("is_production");
        $clientkey = $this->iv->getConfigData("client_key");
        $merchantid = $this->iv->getConfigData("merchant_id");                     
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        
        return [
            'payment' => [
                self::CODE => [
                    'production'=> $production,
                    'clientkey'=> $clientkey,
                    'merchantid' => $merchantid,                   
                    'channels' => $this->iv->getConfigData("payment_channel"),    
                    'ins' => $this->iv->getConfigData("installment_config"),
                    'pay_url' => $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB),
                    'convenience_timeout' => $this->iv->getConfigData("convenience_timeout"),
                    'va_timeout' => $this->iv->getConfigData("va_timeout")
                ]
            ]
        ];
    }

}