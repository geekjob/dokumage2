<?php
/**
 * Doku
 * Copyright (C) 2017 lukluk
 *
 * This file is part of Doku/Snap.
 *
 * Doku/Snap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Doku\Snap\Controller\Payment;

use Magento\Framework\App\Filesystem\DirectoryList;

$object_manager = \Magento\Framework\App\ObjectManager::getInstance();
$filesystem = $object_manager->get('Magento\Framework\Filesystem');
$root = $filesystem->getDirectoryRead(DirectoryList::ROOT);
require_once $root->getAbsolutePath('app/code/Doku/lib/Doku.php');
use \Doku\Snap\Model\Payment\DokuOrder;

class Wallet extends \Magento\Framework\App\Action\Action
{

    protected $_checkoutSession;
    protected $jsonHelper;
    protected $iv;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Doku\Snap\Model\Payment\DokuOrder $iv,
        \Magento\Framework\Json\Helper\Data $jsonHelper
    ) {
        $this->_checkoutSession = $checkoutSession;
        $this->jsonHelper = $jsonHelper;
        $this->iv = $iv;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $om = $this->_objectManager;
            $dokuInit = $om->create('Doku_Initiate');
            $doku_Library = $om->get('Doku_Library');
            $doku_api = $om->get('Doku_Api');
            $config = $om->get('Magento\Framework\App\Config\ScopeConfigInterface');
            $mallid = $config->getValue('payment/dokusnap/merchant_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $key = $config->getValue('payment/dokusnap/client_key', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

            $post = $this->getRequest()->getPost();
            $dokuInit::$sharedKey = $key;
            $dokuInit::$mallId = $mallid;
            $token = $post['doku_token'];

            $pairing_code = $post['doku_pairing_code'];
            $invoice_no = $post['doku_invoice_no'];

            $order = $this->_checkoutSession->getQuote();
            $quoteId = $order->getId();

            $params = array(
                'amount' => $post['doku_amount'], 'invoice' => $invoice_no, 'currency' => '360', 'pairing_code' => $pairing_code, 'token' => $token,
            );
            $words = $doku_Library::doCreateWords($params);
            $basket = $this->iv->getBasket($order, $post['doku_amount']);

            $customer = $this->iv->getCustomer($order);

            $data = array(
                'req_token_id' => $token, 'req_pairing_code' => $pairing_code, 'req_customer' => $customer, 'req_basket' => $basket,
                'req_words' => $words,
            );
            $responsePrePayment = $doku_api::doPrePayment($data);
            if ($responsePrePayment->res_response_code == '0000') {
                //prepayment success
                $dataPayment = array(
                    'req_mall_id' => $mallid,
                    'req_chain_merchant' => 'NA',
                    'req_amount' => $post['doku_amount'],
                    'req_words' => $words,
                    'req_purchase_amount' => $post['doku_amount'],
                    'req_trans_id_merchant' => $invoice_no,
                    'req_request_date_time' => date('YmdHis'),
                    'req_currency' => '360',
                    'req_mobile_phone' => $customer['data_phone'],
                    'req_address' => $customer['data_address'],
                    'req_purchase_currency' => '360',
                    'req_session_id' => sha1(date('YmdHis')),
                    'req_name' => $customer['name'],
                    'req_payment_channel' => 04,
                    'req_basket' => $basket,
                    'req_email' => $customer['data_email'],
                    'req_token_id' => $token,
                );
                $result = $doku_api::doPayment($dataPayment);

                if ($result['res_response_code'] == '0000') {
                    $this->iv->createOrder($quoteId, $invoice_no, true);
                    $result['res_redirect_url'] = $this->iv->getUrl('checkout/onepage/success');
                    $result['res_show_doku_page'] = true;
                    return $this->jsonResponse($result);
                    //success
                } else {
                    if ($result['res_response_msg'] != "INTERNAL_SERVER_ERROR") {
                        $this->iv->createOrder($quoteId, $invoice_no, true);
                    }
                    $result['res_redirect_url'] = $this->iv->getUrl('checkout/onepage/success');
                    $result['res_show_doku_page'] = true;
                    return $this->jsonResponse($result);
                }
            } else {
                //prepayment fail
            }

        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }
}
