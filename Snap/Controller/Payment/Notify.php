<?php
/**
 * Doku
 * Copyright (C) 2017 lukluk
 *
 * This file is part of Doku/Snap.
 *
 * Doku/Snap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Doku\Snap\Controller\Payment;
use \Doku\Snap\Model\Payment\DokuOrder;
class Notify extends \Magento\Framework\App\Action\Action
{

    protected $_checkoutSession;
    protected $jsonHelper;
    protected $iv;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Doku\Snap\Model\Payment\DokuOrder $iv,
        \Magento\Framework\Json\Helper\Data $jsonHelper
    ) {
        $this->_checkoutSession = $checkoutSession;
        $this->iv = $iv;
        $this->jsonHelper = $jsonHelper;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $om = $this->_objectManager;
            $dokuInit = $om->create('Doku_Initiate');  
            $doku_Library = $om->get('doku_Library');  
            $config = $om->get('Magento\Framework\App\Config\ScopeConfigInterface');
            $mallid = $config->getValue('payment/dokusnap/merchant_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $key = $config->getValue('payment/dokusnap/client_key', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

            $dokuInit::$sharedKey=$key;
            $dokuInit::$mallId=$mallid;
            $params = array(
                'amount' => $data['amount'],
                'invoice' => $post['TRANSIDMERCHANT'], 'currency' => '360'
            );
            $words = $doku_Library::doCreateWords($params);
        try {
            $post = $this->getRequest()->getPost();            
            $PAYMENTDATETIME = $post['PAYMENTDATETIME'];
            $PURCHASECURRENCY = $post['PURCHASECURRENCY'];
            $PAYMENTCHANNEL = $post['PAYMENTCHANNEL'];
            $AMOUNT = $post['AMOUNT'];
            $PAYMENTCODE = $post['PAYMENTCODE'];
            $WORDS = $post['WORDS'];
            $RESULTMSG = $post['RESULTMSG'];
            $TRANSIDMERCHANT = $post['TRANSIDMERCHANT'];
            $BANK = $post['BANK'];
            $STATUSTYPE = $post['STATUSTYPE'];
            $APPROVALCODE = $post['APPROVALCODE'];
            $RESPONSECODE = $post['RESPONSECODE'];
            $SESSIONID = $post['SESSIONID'];
            $WORDS_GENERATED = $words;
            if ($WORDS == $WORDS_GENERATED) {
                $n=substr($TRANSIDMERCHANT,0,6);
                $quoteId = intval($n);
                if($RESPONSECODE=="0000") {
                    $this->iv->createOrder($quoteId,$TRANSIDMERCHANT,true);
                    return $this->jsonResponse('success');
                } else {
                    if ($result['RESULTMSG'] != "INTERNAL_SERVER_ERROR"){
                        $this->iv->createOrder($quoteId,$TRANSIDMERCHANT,false);
                        return $this->jsonResponse('INTERNAL_SERVER_ERROR');
                    }
                }
                
                
            } else {
                $this->iv->createOrder($quoteId,$TRANSIDMERCHANT,false);
                return $this->jsonResponse('failed');
            }            
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }
}
