<?php
/**
 * Doku 
 * Copyright (C) 2017 lukluk 
 * 
 * This file is part of Doku/Snap.
 * 
 * Doku/Snap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Doku\Snap\Controller\Payment;
use Magento\Framework\App\Filesystem\DirectoryList;
    $object_manager = \Magento\Framework\App\ObjectManager::getInstance();
    $filesystem = $object_manager->get('Magento\Framework\Filesystem');
    $root = $filesystem->getDirectoryRead(DirectoryList::ROOT);
    require_once($root->getAbsolutePath('app/code/Doku/lib/Doku.php'));


class Index extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $jsonHelper;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $om = $this->_objectManager;
            $dokuInit = $om->create('Doku_Initiate');  
            $doku_Library = $om->get('doku_Library');  
            $config = $om->get('Magento\Framework\App\Config\ScopeConfigInterface');
            $mallid = $config->getValue('payment/dokusnap/merchant_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $key = $config->getValue('payment/dokusnap/client_key', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

            $post = $this->getRequest()->getPost();
            $dokuInit::$sharedKey=$key;
            $dokuInit::$mallId=$mallid;
            $token = $post['doku-token'];
            $pairing_code = $post['doku-pairing-code']; 
            $invoice_no = $post['doku-invoice-no'];
            $session = $om->get('Magento\Checkout\Model\Session');
            $quote2 = $session->getLastRealOrder();
            $orderIncrementId = $quote2->getIncrementId();
            $orderId = $quote2->getId();
            $order = $om->create('Magento\Sales\Model\Order')->load($orderId);
            
            
            $params = array(
            'amount' => $post['doku_amount'], 'invoice' => $invoice_no, 'currency' => '360', 'pairing_code' => $pairing_code, 'token' => $token
            );
            $words = Doku_Library::doCreateWords($params);
            $orderItems = $order->getAllItems();
            foreach($orderItems as $item) {
                $basket[] = array(
                    'name' => $item->getName(), 'amount' => $item->getPrice(), 'quantity' => $item->getQtyOrdered(), 'subtotal' => $item->getSubTotal(),
                );
            }
            
            
            $addr = $order->getShippingAddress()->getData();
            $customer = array(
            'name' => $order->getCustomerFirstname().' '.$order->getCustomerLastname(),
            'data_phone' => $order->getBillingAddress()->getTelephone(), 'data_email' => $order->getCustomerEmail(), 'data_address' => $addr['city']
            );
            

            $dataPayment = array(
            'req_mall_id' => $mallid, 
            'req_chain_merchant' => 'NA',
            'req_amount' => $post['doku_amount'],
            'req_words' => $words, 
            'req_purchase_amount' => '10000.00', 
            'req_trans_id_merchant' => $invoice_no, 
            'req_request_date_time' => date('YmdHis'), 
            'req_currency' => '360', 
            'req_purchase_currency' => '360', 
            'req_session_id' => sha1(date('YmdHis')), 
            'req_name' => $customer['name'], 
            'req_payment_channel' => 15,
            'req_basket' => $basket,
            'req_email' => $customer['data_email'], 
            'req_token_id' => $token,
            );
            $result = Doku_Api::doPayment($dataPayment);
            if($result->res_response_code == '0000'){ 
                echo 'SUCCESS';
                $order->getPayment();
            $payment->setTransactionId($invoice_no)->setIsTransactionClosed(1);
                $order->setStatus(Order::STATE_COMPLETE);
                $order->save();
                return $this->jsonResponse("success");
                //success
            }else{
                $order->getPayment();
            $payment->setTransactionId($invoice_no)->setIsTransactionClosed(1);
                $order->setStatus(Order::STATE_CANCELED);
                $order->save();
                echo 'FAILED';
                return $this->jsonResponse(['error' => true, 'code' => $result->res_response_code]);
            }
            
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {            
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }
}
