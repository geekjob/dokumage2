<?php
/**
 * Doku
 * Copyright (C) 2017 lukluk
 *
 * This file is part of Doku/Snap.
 *
 * Doku/Snap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Doku\Snap\Controller\Payment;

use Magento\Framework\App\Filesystem\DirectoryList;
$object_manager = \Magento\Framework\App\ObjectManager::getInstance();
$filesystem = $object_manager->get('Magento\Framework\Filesystem');
$root = $filesystem->getDirectoryRead(DirectoryList::ROOT);
require_once $root->getAbsolutePath('app/code/Doku/lib/Doku.php');
use \Doku\Snap\Model\Payment\DokuOrder;

class Ib extends \Magento\Framework\App\Action\Action
{

    protected $_checkoutSession;
    protected $iv;
    protected $jsonHelper;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Doku\Snap\Model\Payment\DokuOrder $iv,
        \Magento\Framework\Json\Helper\Data $jsonHelper
    ) {
        $this->_checkoutSession = $checkoutSession;
        $this->jsonHelper = $jsonHelper;
        $this->iv = $iv;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $om = $this->_objectManager;
            $dokuInit = $om->create('Doku_Initiate');
            $doku_Library = $om->get('Doku_Library');
            $doku_api = $om->get('Doku_Api');
            $config = $om->get('Magento\Framework\App\Config\ScopeConfigInterface');
            $mallid = $config->getValue('payment/dokusnap/merchant_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $key = $config->getValue('payment/dokusnap/client_key', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

            $post = $this->getRequest()->getPost();
            $dokuInit::$sharedKey = $key;
            $dokuInit::$mallId = $mallid;

            $cc = str_replace(" - ", "", $_POST['cc_number']);
            $invoice_no = $post['invoice_no'];
            $order = $this->_checkoutSession->getQuote();

            $total = number_format($order->getGrandTotal(), 0) . "00";

            $params = array(
                'amount' => $total, 'invoice' => $invoice_no,
            );
            $words = $doku_Library::doCreateWords($params);
            $basket = $this->iv->getBasket($order, $total);

            $customer = $this->iv->getCustomer($order);

            $dataPayment = array(
                'req_mall_id' => $mallid,
                'req_chain_merchant' => 'NA',
                'req_amount' => $total,
                'req_words' => $words,
                'req_purchase_amount' => $total,
                'req_trans_id_merchant' => $invoice_no,
                'req_request_date_time' => date('YmdHis'),
                'req_currency' => '360',
                'req_purchase_currency' => '360',
                'req_session_id' => sha1(date('YmdHis')),
                'req_name' => $customer['name'],
                'req_payment_channel' => '02',
                'req_mobile_phone' => $customer['data_phone'],
                'req_address' => $customer['data_address'],
                'req_card_number' => $cc,
                'req_basket' => $basket,
                'req_email' => $customer['data_email'],
                'req_response_token' => $_POST['response_token'],
                'req_challenge_code_1' => $_POST['CHALLENGE_CODE_1'],
                'req_challenge_code_2' => $_POST['CHALLENGE_CODE_2'],
                'req_challenge_code_3' => $_POST['CHALLENGE_CODE_3'],
            );

            $result = $doku_api::doDirectPayment($dataPayment);
            if ($result['res_response_code'] == '0000') {
                $this->iv->createOrder($order->getId(), $invoice_no, true);
                $result['res_redirect_url'] = $this->iv->getUrl('checkout/onepage/success');
                $result['res_show_doku_page'] = true;
                return $this->jsonResponse($result);
                //success
            } else {
                if ($result['res_response_msg'] != "INTERNAL_SERVER_ERROR") {
                    $this->iv->createOrder($order->getId(), $invoice_no, false);
                }
                $result['res_redirect_url'] = $this->iv->getUrl('checkout/onepage/success');
                $result['res_show_doku_page'] = true;
                return $this->jsonResponse($result);
            }

        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }
}
