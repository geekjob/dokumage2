<?php
/**
 * Api for doku communications
 */

class Doku_Api
{

    private static function LogApi($url, $request, $response)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
		$rootPath = $directory->getRoot();
		$txt = $url."\n"."REQUEST => ".json_encode($request)."\n RESPONSE => ".json_encode($response);
		$myfile = file_put_contents('/tmp/api-log.txt', $txt.PHP_EOL , FILE_APPEND | LOCK_EX);
    }
    public static function doPrePayment($data)
    {

        $data['req_basket'] = Doku_Library::formatBasket($data['req_basket']);
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $Doku_Initiate = $om->get('Doku_Initiate');
        $ch = curl_init($Doku_Initiate::prePaymentUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'data=' . json_encode($data));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $responseJson = curl_exec($ch);

        curl_close($ch);
		self::LogApi($Doku_Initiate::prePaymentUrl,$data,$responseJson);
        return json_decode($responseJson,true);
    }

    public static function doPayment($data)
    {

        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $data['req_basket'] = Doku_Library::formatBasket($data['req_basket']);
        $Doku_Initiate = $om->get('Doku_Initiate');
        $ch = curl_init($Doku_Initiate::paymentUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'data=' . json_encode($data));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $responseJson = curl_exec($ch);

        curl_close($ch);
		self::LogApi($Doku_Initiate::paymentUrl,$data,$responseJson);

        if (is_string($responseJson)) {
            return json_decode($responseJson,true);
        } else {
            return $responseJson;
        }

    }

    public static function doDirectPayment($data)
    {

        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $data['req_basket'] = Doku_Library::formatBasket($data['req_basket']);
        $Doku_Initiate = $om->get('Doku_Initiate');
        $ch = curl_init($Doku_Initiate::directPaymentUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'data=' . json_encode($data));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $responseJson = curl_exec($ch);

        curl_close($ch);
		self::LogApi($Doku_Initiate::directPaymentUrl,$data,$responseJson);
        if (is_string($responseJson)) {
            return json_decode($responseJson,true);
        } else {
            return $responseJson;
        }

    }

    public static function doGeneratePaycode($data)
    {

        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $data['req_basket'] = Doku_Library::formatBasket($data['req_basket']);
        $Doku_Initiate = $om->get('Doku_Initiate');
        $ch = curl_init($Doku_Initiate::generateCodeUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'data=' . json_encode($data));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $responseJson = curl_exec($ch);

        curl_close($ch);
		self::LogApi($Doku_Initiate::generateCodeUrl,$data,$responseJson);
        if (is_string($responseJson)) {
            return json_decode($responseJson,true);
        } else {
            return $responseJson;
        }

    }
}
