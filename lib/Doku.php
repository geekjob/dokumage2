<?php

if (!function_exists('curl_init')) {
  throw new Exception('CURL PHP extension missing!');
}
if (!function_exists('json_decode')) {
  throw new Exception('JSON PHP extension missing!');
}
use Magento\Framework\App\Filesystem\DirectoryList;
$object_manager = \Magento\Framework\App\ObjectManager::getInstance();
$filesystem = $object_manager->get('Magento\Framework\Filesystem');
$root = $filesystem->getDirectoryRead(DirectoryList::ROOT);
 

require_once($root->getAbsolutePath('app/code/Doku/lib/Initiate.php'));
require_once($root->getAbsolutePath('app/code/Doku/lib/Api.php'));
require_once($root->getAbsolutePath('app/code/Doku/lib/Library.php'));
?>